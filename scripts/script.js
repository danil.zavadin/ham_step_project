
$(function () {

    //TABS FUNCTIONALITY

    $(".tab-title").click(function (e) {
        const clickedTab = this
        $.each($(".tab-title"), function (e) {
            this.classList.remove("active")
        })
        clickedTab.classList.add("active")
        console.log(clickedTab);
        $.each($(".tab-content"), function () {
            $.style(this, "display", "none")
            if (clickedTab.dataset.id === this.dataset.id) {
                $.style(this, "display", "flex")
            }
        })
    })

    //FILTER FUNCTIONALITY

    $(".filter-tab-item").click(function (){
        if ($(this).data("id") !== "all") {
            $(".filter-image-item").hide().filter("." + $(this).data("id")).slideToggle(1000)
        }else{
            $(".filter-image-item").toggle()
        }
    })

    $('.load-more-button').click(function () {
        $('.filter-images li:hidden').slice(0, 12).slideToggle();
        if ($('.filter-images li').length === $('.filter-images li:visible').length) {
            $('.load-more-button').hide();
        }
    })

    $('.slider-nav').slick({
        asNavFor: ".slider-content",
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="slick-prev"> < </button>',
        nextArrow: '<button type="button" class="slick-next"> > </button>',
        arrows: true,
        focusOnSelect: true,
    });
    $('.slider-content').slick({
        asNavFor: '.slider-nav',
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
    });

})